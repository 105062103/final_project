var little = 10;
var txt_height = 20;
var limit = 10;
var playState = {
    create: function(){
        this.graphics = game.add.graphics(0, 0);
        this.graphics.lineStyle(2, 0x666666, 1);
        this.graphics.drawRoundedRect(little, game.height-3*little-txt_height,
                                      game.width/2-2*little, 2*little+txt_height, 10);
        var g = game.add.graphics(0, 0);
        g.lineStyle(2, 0x777777, 1);
        g.drawRect(2*little, game.height-2*little-txt_height,
                    game.width/2-4*little,txt_height);

        this.userref = firebase.database().ref("game_rooms/"+game.global.room_name+"/"+user_name);
        this.talking = false;
        this.talk_to = "";
        this.talk_to_ref = "";
        this.talk_box_txt = "";
        this.talk_box = 0;
        this.message_array = [ 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0];
        this.setkeys();

        this.userref.child("message").orderByChild("createdAt").on("value",playState.get_message);
    },
    setkeys: function(){
        var keyt = game.input.keyboard.addKey(Phaser.Keyboard.T);//talk
        keyt.onDown.add(function(){ this.change_mode(); }, this); 
        var keyy = game.input.keyboard.addKey(Phaser.Keyboard.Y);//sent
        keyy.onDown.add(this.sent_message, this); 
        
        this.key1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.key1.onDown.add(function() {this.using_number_key(1);}, this); 
        this.key2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.key2.onDown.add(function() {this.using_number_key(2);}, this); 
        this.key3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        this.key3.onDown.add(function() {this.using_number_key(3);}, this); 
        this.key4 = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
        this.key4.onDown.add(function() {this.using_number_key(4);}, this); 
        this.key5 = game.input.keyboard.addKey(Phaser.Keyboard.FIVE);
        this.key5.onDown.add(function() {this.using_number_key(5);}, this); 
        this.key6 = game.input.keyboard.addKey(Phaser.Keyboard.SIX);
        this.key6.onDown.add(function() {this.using_number_key(6);}, this); 
        this.key7 = game.input.keyboard.addKey(Phaser.Keyboard.SEVEN);
        this.key7.onDown.add(function() {this.using_number_key(7);}, this); 
        this.key8 = game.input.keyboard.addKey(Phaser.Keyboard.EIGHT);
        this.key8.onDown.add(function() {this.using_number_key(8);}, this); 
    },
    change_mode:function(){
        var i;
        this.talking = !this.talking;
        this.graphics.destroy();
        this.graphics = game.add.graphics(0, 0);
        this.graphics.lineStyle(2, 0x666666, 1);
        if(this.talking){
            this.graphics.drawRoundedRect(little, game.height/2+little,
                                          game.width/2-2*little, game.height/2-2*little, 10);
            for(i=0;i<limit;i++) if(this.message_array[i]) this.message_array[i].visible = true;
        }
        else {
            this.graphics.drawRoundedRect(little, game.height-3*little-txt_height,
                                          game.width/2-2*little, 2*little+txt_height, 10);
            for(i=0;i<limit;i++) if(this.message_array[i]) this.message_array[i].visible = false;
        }
    },
    using_number_key: function(number_key){
        if(this.talking){
            if( number_key < 5 ){
                if( number_key - 1 != game.global.my_number ){
                    if(game.global.player[number_key-1]){
                        this.talk_to = game.global.player[number_key-1];
                        this.talk_box_txt = "To " + this.talk_to + " : ";
                    }
                }
            }
            else{
                var message;
                switch(number_key){
                    case 5: message = "wow"; break;
                    case 6: message = "ya"; break;
                    case 7: message = "hello"; break;
                    case 8: message = "nooooooo"; break;
                }
                if(this.talk_box_txt) this.talk_box_txt = this.talk_box_txt.split(':')[0] + ": " + message;
            }
            this.reset_talk_box();
        }
    },
    get_message: function(snapshot) {
        var num = snapshot.numChildren();
        var i = 0;
        snapshot.forEach(function(childSnapshot) {
            if( num-i > limit ) 
            firebase.database().ref("game_rooms/"+game.global.room_name+"/"+user_name+
                                    "/message/"+childSnapshot.key).remove();
            else{
                var childData = childSnapshot.val();
                var txt = ( ( childData.player==user_name ) ? "" : childData.player + " : ") + childData.text;
                if(playState.message_array[i]) playState.message_array[i].destroy();
                playState.message_array[i] = game.add.text( 2*little , game.height-2*little-txt_height*(num-i+2)+3,
                                                            txt , { font: '18px Arial', fill: '#ffffff' });
                if(!playState.talking){
                    playState.message_array[i].visible = false;
                }
            }
            i++;
        });
    },
    sent_message: function(){
        if(this.talk_box_txt){
            currenttime = firebase.database.ServerValue.TIMESTAMP;
            this.talk_to_ref = firebase.database().ref("game_rooms/" + game.global.room_name + "/" + 
                                                        this.talk_to + "/message");
            var talk_box_txt = this.talk_box_txt;
            this.talk_to_ref.push().set({
                'player': user_name,
                'text': talk_box_txt.split(':')[1],
                'createdAt': currenttime
            })
            this.userref.child("message").push().set({
                'player': user_name,
                'text': talk_box_txt,
                'createdAt': currenttime
            })
            this.talk_box_txt = this.talk_box_txt.split(':')[0] + ": ";
            this.reset_talk_box();
        }
    },
    reset_talk_box: function(){
        if(this.talk_box) this.talk_box.destroy();
        this.talk_box = game.add.text(2*little, game.height-2*little-txt_height+3,
                                      this.talk_box_txt, { font: '18px Arial', fill: '#ffffff' });
    }
};