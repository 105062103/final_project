function initApp() {
    if(firebase.auth().currentUser) window.location.href="index.html"
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnReset = document.getElementById('btnReset');

    btnLogin.addEventListener('click', function () {
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(function(){
            return firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(
                function(e){
                    window.location.href="index.html"
                ;}
                ).catch(
                function(e){create_alert("error",e);
                });
        });
    });

    var provider = new firebase.auth.GoogleAuthProvider();
    btnGoogle.addEventListener('click', function (e) {
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(function(){
            return firebase.auth().signInWithPopup(provider).then(
                function(result){window.location.href="index.html";}
                ).catch(
                function(error){create_alert("error",error);
                });
        });
    });

    btnSignUp.addEventListener('click', function () {   
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(function(){
        return firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value)
            .then(function(e){
                create_alert("success","Success!");
            }).catch(function(e){create_alert("error",e);
            });
        });
    });

    btnReset.addEventListener('click', function () {  
        firebase.auth().sendPasswordResetEmail(txtEmail.value)
        .then(function() {
            create_alert("success","更改密碼Email已發送");
            txtPassword.value = "";
            }).catch(
            function(e) {create_alert("error",e);
        });
    });
}

// Custom alert
function create_alert(type,text) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>"+text+"</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>"+text+"</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};