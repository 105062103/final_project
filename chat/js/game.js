function changeto_gamemode(){
    document.getElementById('chat').innerHTML = 
    '<div class="container text-center">'+
        '<div id="canvas">'+
            '<p class="setting-form">'+
                '<a>Which room do you want to enter?</a>'+
                '<input type="text" id="room_name" class="form-control" placeholder="" autofocus>'+
                '<button class="btn btn-lg btn-primary btn-block" onclick="gammode()">sent</button>'+
            '</p>'+
        '</div>'+
   '</div>';
}
function gammode(){
    var txtname = document.getElementById('room_name');
    if(!txtname.value) alert("Please enter room name");
    else{
        document.getElementById('chat').innerHTML = "<div id='canvas'></div>"
        // Initialize Phaser 
        game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
        // Define our global variable
        game.global = { room_name : "" , player : ["","","",""] , my_number : 0 }; 
        game.global.room_name = txtname.value;
        // Add all the states 
        game.state.add('boot', bootState); 
        game.state.add('load', loadState); 
        game.state.add('waiting', waitingState); 
        game.state.add('play', playState); 
        // Start the 'boot' state 
        game.state.start('boot');
    }
}