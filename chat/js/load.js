var loadState = { 
    preload: function () {
        // Add a 'loading...' label on the screen 
        var loadingLabel = game.add.text(game.width/2, 90+150, 'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar 
        var progressBar = game.add.sprite(game.width/2, 90+200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);
        game.load.audio('audio1', 'img/bgm1.ogg');
    }, 
    update: function() { 
        game.state.start('waiting');
    }
};